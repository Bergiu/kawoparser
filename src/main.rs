extern crate reqwest;
extern crate select;

use reqwest::header::{COOKIE, SET_COOKIE};
use reqwest::{Client, ClientBuilder, RedirectPolicy, Response};
use select::document::Document;
use select::predicate::{And, Attr, Child, Class, Name, Not};
use std::error::Error;
use std::io::Read;
use std::vec::Vec;

fn get_csrf(html: &str) -> String {
    let document = Document::from(html);
    let tokens = document
        .find(Child(
            And(Name("form"), Class("form-green")),
            And(Name("input"), Attr("name", "csrfmiddlewaretoken")),
        ))
        .filter_map(|n| n.attr("value"));
    let mut csrftoken = String::new();
    for token in tokens {
        csrftoken = token.to_string();
    }
    csrftoken
}

fn get_cookies(res: &Response) -> Result<String, Box<Error>> {
    let mut cookies = String::new();
    let set_cookies = res.headers().get_all(SET_COOKIE);
    for cookie in set_cookies {
        let tmp: String = cookie.to_str()?.to_string();
        let mut tmp = tmp.split(';');
        let s = tmp.next().unwrap();
        cookies += s;
        cookies += ";";
    }
    Ok(cookies)
}

fn pre_login() -> Result<(String, String), Box<Error>> {
    // get main page
    let mut res = reqwest::get("https://mein.kawo2.rwth-aachen.de/auth/")?;
    // get csrftoken
    let mut buffer = String::new();
    res.read_to_string(&mut buffer)?;
    // get cookies
    Ok((get_csrf(&buffer), get_cookies(&res)?))
}

fn login(username: String, password: String) -> Result<String, Box<Error>> {
    let (csrf, cookies) = pre_login()?;
    // login with csrftoken, username and password
    let client = ClientBuilder::new()
        .redirect(RedirectPolicy::none())
        .build()
        .unwrap();
    let params = [
        ("csrfmiddlewaretoken", csrf),
        ("username", username),
        ("password", password),
    ];
    let res = client
        .post("https://mein.kawo2.rwth-aachen.de/auth/")
        .form(&params)
        .header(COOKIE, cookies)
        .send()?;
    // get cookies
    Ok(get_cookies(&res)?)
}

struct Abstract {
    balance: String,
}

fn get_abstract(cookies: &str) -> Result<Abstract, Box<Error>> {
    let mut res = Client::new()
        .get("https://mein.kawo2.rwth-aachen.de")
        .header(COOKIE, cookies)
        .send()?;
    let mut buffer = String::new();
    res.read_to_string(&mut buffer)?;
    let html: &str = &buffer;
    let document = Document::from(html);
    let filtered = document.find(Child(
        And(Name("div"), Class("panel-body")),
        And(Name("h1"), Class("text-center")),
    ));
    let mut balance = String::new();
    for item in filtered {
        balance = item.text();
    }
    let abs = Abstract { balance };
    Ok(abs)
}

struct PersonalData {
    forename: String,
    surname: String,
    birthday: String,
    street: String,
    housenumber: String,
    plz_town: String,
    roomnumber: String,
    email: String,
}

fn get_personal_data(cookies: &str) -> Result<PersonalData, Box<Error>> {
    let mut res = Client::new()
        .get("https://mein.kawo2.rwth-aachen.de/personal_data")
        .header(COOKIE, cookies)
        .send()?;
    let mut buffer = String::new();
    res.read_to_string(&mut buffer)?;
    let html: &str = &buffer;
    let document = Document::from(html);
    let filtered = document.find(Child(
        Child(And(Name("div"), Class("panel-body")), Name("p")),
        Not(Name("strong")),
    ));
    let mut text: String;
    let mut vec = Vec::new();
    for item in filtered {
        text = item.text().trim().to_string();
        if !text.is_empty() {
            vec.push(text);
        }
    }

    let personal_data = PersonalData {
        forename: vec[0].clone(),
        surname: vec[1].clone(),
        birthday: vec[2].clone(),
        street: vec[3].clone(),
        housenumber: vec[4].clone(),
        plz_town: vec[5].clone(),
        roomnumber: vec[6].clone(),
        email: vec[7].clone(),
    };
    Ok(personal_data)
}

fn main() -> Result<(), Box<Error>> {
    let cookies = login("".to_string(), "".to_string())?;

    let abs = get_abstract(&cookies)?;
    let pd = get_personal_data(&cookies)?;
    println!("Vorname:    {}", pd.forename);
    println!("Nachname:   {}", pd.surname);
    println!("Geburtstag: {}\n", pd.birthday);
    println!( "Straße:       {} {}", pd.street, pd.housenumber);
    println!("PLZ & Stadt:  {}", pd.plz_town);
    println!("Zimmernummer: {}\n", pd.roomnumber);
    println!("E-Mail-Adresse: {}\n", pd.email);
    println!("Guthaben: {}", abs.balance);

    Ok(())
}
